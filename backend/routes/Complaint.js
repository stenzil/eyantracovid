const router = require("express").Router();

let Complaint = require("../models/complaint.model");
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  },
});
const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  storage: storage,
  fileFilter: fileFilter,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
});
router.route("/").get((req, res) => {
  Complaint.find()
    .sort({ weight: -1 })
    .then((complaints) => res.json(complaints))
    .catch((err) => res.status(400).json("Error :: " + err));
});

router.route("/upvote/:id").get((req, res) => {
  Complaint.findById(req.params.id)
    .then((complaint) => {
      complaint.weight = complaint.weight + 1;
      complaint
        .save()
        .then(() => res.json("Weight Incremented"))
        .catch((err) => res.status(400).json("Err ::" + err));
    })
    .catch((err) => res.status(400).json("Error :: " + err));
});
router.route("/trivial/:id").get((req, res) => {
  Complaint.findById(req.params.id)
    .then((complaint) => {
      complaint.weight = complaint.weight - 1;
      complaint
        .save()
        .then(() => res.json("Weight Decremented"))
        .catch((err) => res.status(400).json("Err ::" + err));
    })
    .catch((err) => res.status(400).json("Error :: " + err));
});

router.route("/").post(upload.single("image"), (req, res) => {
  const name = req.body.name;
  const caption = req.body.caption;
  const people_count = req.body.people_count;
  const mobile = req.body.mobile;
  const location = req.body.location;
  const area = req.body.area;

  //const image = req.file.path;

  const newComplaint = new Complaint({
    name,
    caption,
    people_count,
    mobile,
    location,
    area,

    // image,
  });
  newComplaint
    .save()
    .then(() => res.json("Posted"))
    .catch((err) => res.status(400).json("Error :: " + err));
});

module.exports = router;
