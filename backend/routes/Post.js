const reta = require("./CoronaCount");

const router = require("express").Router();
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads/");
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  },
});
const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  storage: storage,
  fileFilter: fileFilter,
  limits: {
    fileSize: 1024 * 1024 * 5,
  },
});
let Post = require("../models/post.model");

router.route("/").get((req, res) => {
  Post.find()
    .sort({ weight: -1 })
    .then((posts) => res.json(posts))
    .catch((err) => res.status(400).json("Error :: " + err));
});

router.route("/upvote/:id").get((req, res) => {
  Post.findById(req.params.id)
    .then((post) => {
      post.weight = post.weight + 1;
      post
        .save()
        .then(() => res.json("Weight Incremented"))
        .catch((err) => res.status(400).json("Err ::" + err));
    })
    .catch((err) => res.status(400).json("Error :: " + err));
});
router.route("/trivial/:id").get((req, res) => {
  Post.findById(req.params.id)
    .then((post) => {
      post.weight = post.weight - 1;
      post
        .save()
        .then(() => res.json("Weight Decremented"))
        .catch((err) => res.status(400).json("Err ::" + err));
    })
    .catch((err) => res.status(400).json("Error :: " + err));
});
router.route("/count").get((req, res) => {
  const data = reta
    .reta()
    .then((posts) => res.json(posts))
    .catch((err) => res.status(400).json("Error :: " + err));
});
router.route("/news").get((req, res) => {
  const data = reta
    .news()
    .then((posts) => res.json(posts))
    .catch((err) => res.status(400).json("Error :: " + err));
});

router.route("/twe").get((req, res) => {
  const data = reta
    .twe()
    .then((posts) => res.json(posts))
    .catch((err) => res.status(400).json("Error :: " + err));
});

router.route("/").post(upload.single("image"), (req, res) => {
  const name = req.body.name;
  const caption = req.body.caption;
  const adharno = req.body.adharno;
  const mobile = req.body.mobile;
  const location = req.body.location;
  const weight = req.body.weight;
  const category = req.body.category;
  const address = req.body.address;
  //const image = req.file.path;

  const newPost = new Post({
    name,
    caption,
    adharno,
    mobile,
    location,
    weight,
    category,
    address,
    // image,
  });
  newPost
    .save()
    .then(() => res.json("Posted"))
    .catch((err) => res.status(400).json("Error :: " + err));
});

module.exports = router;
