const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const postSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      minlength: 3,
    },
    caption: {
      type: String,
      maxlength: 300,
    },
    image: {
      type: String,
    },
    location: {
      type: Array,
    },
    address: {
      type: String,
    },
    adharno: {
      type: Number,
    },
    category: {
      type: String,
      required: true,
    },
    mobile: {
      type: Number,

      minlength: 10,
      maxlength: 10,
    },
    weight: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
  }
);

const Post = mongoose.model("Post", postSchema);
module.exports = Post;
