const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ComplaintSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      minlength: 3,
    },
    people_count: {
      type: Number,
    },
    caption: {
      type: String,
      maxlength: 300,
    },
    image: {
      type: String,
    },
    location: {
      type: Array,
    },
    area: {
      type: String,
    },

    mobile: {
      type: Number,
      minlength: 10,
      required: true,
      maxlength: 10,
      minlength: 10,
    },
    weight: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
  }
);

const Complaint = mongoose.model("Complaint", ComplaintSchema);
module.exports = Complaint;
