const express = require("express");
const MongoClient = require("mongoose");
const bodyparser = require("body-parser");
const cors = require("cors");
require("dotenv").config();

const app = express();
const port = process.env.PORT || 8000;
app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI;

MongoClient.connect(uri, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
});
const connection = MongoClient.connection;
connection.once("open", () => {
  console.log("Connected to db");
});
const complaintrouter = require("./routes/Complaint");

const postrouter = require("./routes/Post");
app.use("/uploads", express.static("uploads"));
app.use("/posts", postrouter);
app.use("/cmp", complaintrouter);

app.listen(port, () => {
  console.log("we are live on " + port);
});
