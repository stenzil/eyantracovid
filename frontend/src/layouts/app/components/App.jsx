// @flow

import * as React from "react";

import "./styles.scss";
type Props = {
  children: React.Node,
};

const App = ({ children }: Props) => (
  <>
    <main className="min-vh-100">{children}</main>
    <footer className="py-5 footer text-dark text-center">
      <p>MADE FOR EYANTRA HACKATHON 2020</p>
      <small className="text-capitalize">
        By Praveen Pandey ( EYANTRA ROBOTICS 2018 TEAM LEADER - completion
        Certificate Holder )
      </small>
    </footer>{" "}
  </>
);

export default App;
