// @flow
import * as React from 'react';
import App from '../components/App';

type Props = {
  children: React.Node,
};

const AppContainer = (props: Props) => <App {...props} />;

export default AppContainer;
