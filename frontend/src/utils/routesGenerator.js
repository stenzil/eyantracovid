// @flow
import { uniqBy } from "lodash";
import type { RoutesType } from "../types";

export function getFooterRoutes(): RoutesType {
  return [
    ...getCommonRoutes(),
    {
      title: "Partner With Us",
      path: "/partner-with-us",
      exact: true,
      component: () => import("../routes/PartnerWithUs")
    },
    {
      title: "Investors",
      path: "/investors",
      exact: true,
      component: () => import("../routes/Investors")
    },
    { title: "Download", path: "#download", exact: true },
    { title: "Contest", path: "/contest", exact: true },
    {
      title: "Careers",
      path: "/careers",
      exact: true
    },
    {
      title: "Contact Us",
      path: "/contact-us",
      exact: true,
      component: () => import("../routes/ContactUs")
    },
    {
      title: "About Us",
      path: "/about",
      exact: true,
      component: () => import("../routes/About")
    },
    {
      title: "FAQs",
      path: "/faqs",
      exact: true,
      component: () => import("../routes/FAQs")
    },

    {
      title: "Privacy Policy",
      path: "/privacy-policy",
      exact: true,
      component: () => import("../routes/PrivacyPolicy")
    },
    {
      title: "Terms of Services",
      path: "/terms-of-services",
      exact: true,
      component: () => import("../routes/TermsOfServices")
    },
    {
      title: "@email",
      path: "/"
    },
    {
      title: "1800 270 232",
      path: "/"
    }
  ];
}

export function getAppRoutes(): RoutesType {
  return uniqBy([...getNavBarRoutes(), ...getFooterRoutes()], "path");
}
