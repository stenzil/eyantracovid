function actionCreator(module, constants) {
  return constants.reduce((actions, constant) => {
    actions[constant] = `Eyantra/${module.toLowerCase()}/${constant}`;
    return actions;
  }, {});
}

export default actionCreator;
