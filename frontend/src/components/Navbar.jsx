// @flow
import React, { useEffect } from "react";
import { Link } from "react-router-dom";

type Props = {
  children: React.Node,
  bg: number,
  check: number,
};

const Navbar = ({ children, bg = 0, check }: Props) => {
  useEffect(() => {
    const header = document.querySelector(".header");
    window.onscroll = function () {
      bg = this.window.scrollY;

      if (bg > 200) {
        header.classList.add("active");
      } else {
        header.classList.remove("active");
      }
    };
  });
  return (
    <header
      className={
        check === 12 ? "header fixed-top bg-white" : "header fixed-top"
      }
    >
      <nav className="container navbar navbar-expand-md">
        <Link className="navbar-brand" to="/">
          <img src="/img/wh.svg" width="250" alt="logo" />
        </Link>

        {children}
      </nav>
    </header>
  );
};

export default Navbar;
