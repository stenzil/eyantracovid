// @flow
import React from "react";
import Icon from "./Icon";

const Loader = () => (
  <div className="d-flex align-items-center justify-content-center min-vh-100 bg-light">
    <Icon name="fa fa-circle-o-notch fa-spin fa-3x text-secondary" />
  </div>
);

export default Loader;
