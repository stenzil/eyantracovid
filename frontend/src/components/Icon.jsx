// @flow
import React from "react";

type Props = {
  name: string,
  color?: string
};

const Icon = ({ name, color }: Props) => (
  <>
    <i style={{ color: color }} className={`fa ${name} fa-fw `} />
  </>
);
export default Icon;
