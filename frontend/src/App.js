// @flow
import React from "react";

import { Router } from "react-router";

import Routes from "./routes/Routes";
import history from "./utils/history";
import "./styles/master.scss";

const App = () => (
  <Router history={history}>
    <Routes />
  </Router>
);

export default App;
