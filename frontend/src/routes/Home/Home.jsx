// @flow
import React, { Component } from "react";

import { Element, Link as ScrollLink } from "react-scroll";
import Zoom from "react-reveal/Fade";
import axios from "axios";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Navbar } from "../../components";
import "./styles.scss";
import Flip from "react-reveal/Flip";

class Home extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      count: [],
      noofp: 0,
      news: [],
      tweets: [],
      status: "",
      category: [
        "I need food / भोजन चाहिए",
        "I need shelter / आश्रय चाहिए",
        "Emergency / आपातकालीन स्थिति",
        "I'm out of grocery / राशन की कमी",
        "I'm not feeling well / तबियत ठीक नहीं",
        "Other / अन्य समस्या        ",
      ],
      address: "",
      prevdate: "",
      latitude: null,
      longitude: null,
      name: "",
      adhar: 0,
      caption: "",
      mobile: 0,
      image: "",
    };
  }
  async onSubmit() {
    const url = "http://localhost:8000/posts";

    var payload = {
      name: this.state.name,
      caption: this.state.caption,
      adharno: this.state.adhar,
      mobile: this.state.mobile,
      location: [this.state.longitude, this.state.latitude],
      address: this.state.address,
      weight: this.state.weight,
      category: this.state.status,
      image: this.state.image,
    };
    axios
      .post(url, payload)
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  async complain() {
    const url = "http://localhost:8000/cmp";
    var payload = {
      name: this.state.name,
      caption: this.state.caption,
      area: this.state.address,
      mobile: this.state.mobile,
      location: [this.state.longitude, this.state.latitude],
      image: this.state.image,
      people_count: this.state.noofp,
    };
    axios
      .post(url, payload)
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  updatestatus(e) {
    //console.log(e);
    this.setState({
      status: e,
    });
  }

  async componentDidMount() {
    const url = "http://localhost:8000/posts/count";
    const tweetsurl = "http://localhost:8000/posts/twe";
    const newsurl = "http://localhost:8000/posts/news";
    const response = await fetch(url);
    const data = await response.json();
    const tweetsresponse = await fetch(tweetsurl);
    const tweetsdata = await tweetsresponse.json();
    const newsresponse = await fetch(newsurl);
    const newsdata = await newsresponse.json();
    var date = new Date();
    date.setDate(date.getDate() - 1);
    date = date.toISOString();
    await navigator.geolocation.getCurrentPosition((position) =>
      this.setState({
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
        count: data,
        prevdate: date,
        loading: false,
        status: this.state.category[0],
        news: newsdata,
        tweets: tweetsdata,
      })
    );
  }

  handleFileUpload = async (e) => {
    console.log(e.target.files[0]);
  };
  handleInputChange = async (event) => {
    event.preventDefault();
    console.log(event.target.name + "::::" + event.target.value);
    await this.setState({
      [event.target.name]: event.target.value,
    });
  };
  render() {
    return (
      <div>
        <Navbar>
          <ul className=" d-flex align-items-around navbar-nav ml-auto text-capitalize text-white ">
            <li className="nav-item pl-4 d-lg-block d-none">
              <ScrollLink className="nav-link text-white " to="spread" smooth>
                The Spread
              </ScrollLink>
            </li>
            <li className="nav-item pl-4 d-lg-block d-none">
              <ScrollLink className="nav-link text-white " to="complain" smooth>
                Complain
              </ScrollLink>
            </li>

            <li>
              <ScrollLink
                to="help"
                smooth
                className="d-lg-block d-none nav-link btn btn-primary rounded-pill border-white bg-transparent px-4 ml-4"
              >
                Give Help
              </ScrollLink>
            </li>
            <li className="nav-item ml-4">
              <ScrollLink
                className="d-lg-block d-none text-dark px-4 btn btn-primary text-capitalize rounded-pill shadow bg-white border-white "
                to="help"
                smooth
              >
                Need Help ?
              </ScrollLink>
            </li>
          </ul>
        </Navbar>
        <div className="">
          <Carousel
            className="fluid"
            showArrows={false}
            showStatus={false}
            showIndicators={false}
            showThumbs={false}
            infiniteLoop={true}
            interval={7000}
            autoPlay={true}
            stopOnHover={false}
          >
            <div className=" d-md-block">
              <a href="/posts">
                <video
                  autoPlay
                  muted
                  loop
                  style={{
                    width: "100%",
                    left: 0,
                    top: 0,
                  }}
                >
                  <source src="/img/csd.mp4" type="video/mp4" />
                </video>
              </a>
            </div>

            <div className=" d-md-block">
              <a href="/skjn">
                <img src="/img/poster5.png" alt=".."></img>
              </a>
            </div>

            <div className="row py-5 mt-5 px-4">
              <div className="col-4">
                <img src="img/poster1.png" alt="" />
              </div>
              <div className="col-4">
                <img src="img/poster2.png" alt="" />
              </div>
              <div className="col-4">
                <img src="img/poster3.png" alt="" />
              </div>
            </div>
            <div className="row mt-5 py-5 px-4">
              <div className="col-6">
                <img src="img/poster4.png" alt="" />
              </div>
              <div className="col-6 col-md-4 my-auto mx-auto">
                <video
                  autoPlay
                  muted
                  loop
                  style={{
                    width: "100%",
                    left: 0,
                    top: 0,
                  }}
                >
                  <source src="/img/pny.mp4" type="video/mp4" />
                </video>
              </div>
            </div>
          </Carousel>
        </div>
        <section className="banner min-vh-100  py-1 ">
          <div className="container text-center ">
            <div className="row justify-content-around py-auto">
              <div className="col-12 col-md-6 ">
                <div className="row">
                  <div className="col-12 mt-5 py-5">
                    <h1 className="text-white text-left mt-5 pt-5 heading">
                      Stay Inside, Stand Together
                    </h1>
                  </div>
                </div>
                <div className="row ">
                  <div className=" col-12 description nunito ">
                    <p className="text-white text-capitalize text-left ">
                      You can find people who are facing hunger crisis, If
                      you've a little extra , help those who have nothing right
                      now. Daily wagers are on roads walking mile in search of
                      food and livelihood. Let's help them. Find people's ADHAR
                      Number and Send Money Via Bhim UPI directly into their
                      account.
                    </p>
                  </div>
                </div>
                <div className="row ml-1 pt-3">
                  <p className="pb-5">
                    <a
                      className=" px-5 btn bg-white text-left rounded-pill shadow"
                      href="/posts"
                    >
                      HELP
                    </a>
                  </p>
                </div>
              </div>
              <div className="col-12 col-md-6 mt-md-5 py-md-5 ">
                <div className="row justify-content-center">
                  <Zoom right>
                    <img
                      alt=".."
                      className="mt-5 mb-4 img-fluid fade-in "
                      src={"/img/bhim_aadhar_logo.png"}
                    />
                  </Zoom>
                </div>
                <div className="row  justify-content-center description nunito ">
                  <p className="text-black text-capitalize pb-4">
                    The needy people post their problem here, Scroll through the
                    posts and help atleast one of them, Spread happiness and
                    help them staying indoors.
                  </p>
                  <h2 className="text-danger text-center">
                    {" "}
                    It's a war, we've to fight it togehter. You can't roam
                    around with other's inffected.{" "}
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className="container">
            {this.state.loading ? (
              <div className="col-12 text-center">
                <div className="text-center">
                  <img src="/img/loader.gif" alt=""></img>
                </div>
              </div>
            ) : (
              <div className="row black justify-content-around mt-5 py-5">
                <div className="col-5 text-center text-dark shadow border">
                  <h2 className="py-4">Tweets By Prime Minister</h2>
                  {this.state.tweets.slice(0, 5).map((data, tweets_i) => (
                    <a href={"https" + data.split("https")[1]}>
                      <p>{data.split("https")[0]}</p>
                    </a>
                  ))}
                </div>
                <div className="col-5 text-center text-dark shadow border">
                  <h2 className="py-4">Tweets on COVID-19</h2>
                  {this.state.tweets.slice(5, 9).map((data, tweets_i) => (
                    <p className="text-primary">{data}</p>
                  ))}
                </div>
              </div>
            )}
          </div>
        </section>
        <section className="min-vh-100  pt-5">
          <div className="container">
            <div className="row mt-5 pt-5">
              {this.state.category.map((data, _i) => (
                <div className="col-6 col-md-4 pt-5">
                  <Flip>
                    <ScrollLink
                      key={_i}
                      name="status"
                      onClick={() => this.updatestatus(data)}
                      smooth
                      to="help"
                    >
                      <div className="align-item-center text-wrap">
                        <div
                          className="card border-circle shadow-lg text-dark my-auto"
                          style={{
                            backgroundColor: "rgb(141, 215, 228)",
                            height: "8.5rem",
                          }}
                        >
                          <div className="card-body rounded">
                            <div className="text-wrap  text-center">
                              <h2
                                style={{ fontSize: "2rem" }}
                                className=" nav-link"
                              >
                                {data}
                              </h2>
                            </div>
                          </div>
                        </div>
                      </div>
                    </ScrollLink>
                  </Flip>
                </div>
              ))}
            </div>
          </div>
        </section>
        <Element name="complain">
          <section>
            <div className="container">
              <div className="row justify-content-around mt-5 py-5">
                <div className="col-4 description nunito mt-5">
                  <h1 className="py-4 text-danger" style={{ fontSize: "5rem" }}>
                    Complain
                  </h1>
                  <p className="text-large">
                    If any one in your area is moving out unnecessarily, Make a
                    post, let the world know and make them realize their fault,
                    Make it easy for the police to trace such bottlenecks.
                    <i className="text-success">
                      {" "}
                      Your Identity will be kept secret
                    </i>
                  </p>
                </div>
                <div className="col-7 my-auto pt-5">
                  <form className="mt-3">
                    <div className="form-row ">
                      <div class="form-group col-5">
                        <input
                          type="password"
                          class="form-control"
                          name="name"
                          onChange={this.handleInputChange}
                          placeholder="Enter your Name (Hidden)"
                        ></input>
                      </div>
                      <div class="form-group col-5">
                        <input
                          class="form-control mobile"
                          type="password"
                          name="mobile"
                          placeholder="Mobile Number (Hidden)"
                          onChange={this.handleInputChange}
                        ></input>
                      </div>
                      <div class="form-group col-5">
                        <input
                          class="form-control"
                          name="address"
                          onChange={this.handleInputChange}
                          placeholder="Area"
                        ></input>
                      </div>
                      <div class="form-group col-5">
                        <input
                          class="form-control"
                          name="noofp"
                          onChange={this.handleInputChange}
                          placeholder="Person Count"
                        ></input>
                      </div>
                      <div class="custom-file ml-1 col-6 mb-3">
                        <input
                          type="file"
                          name="image"
                          enctype="multipart/form-data"
                          onChange={this.handleFileUpload}
                          placeholder="Upload Image"
                          aria-describedby="inputGroupFileAddon01"
                        ></input>
                        <label
                          class="custom-file-label"
                          for="inputGroupFile01"
                        ></label>
                      </div>
                      <div class="form-group col-10">
                        <textarea
                          class="form-control"
                          name="caption"
                          rows="3"
                          onChange={this.handleInputChange}
                          placeholder="Mention / वर्णन"
                        ></textarea>
                      </div>
                      <div className="col-10">
                        <button
                          type="button"
                          class="btn btn-primary btn-lg btn-block "
                          onClick={this.complain.bind(this)}
                          href="/posts"
                        >
                          <a href="/posts" className="text-white ">
                            Submit
                          </a>
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="-300 0 950 270"
              className="svg"
            >
              <path
                d="M-314,267 C105,364 400,100 812,279"
                fill="none"
                stroke="black"
                strokeWidth="120"
                strokeLinecap="round"
              />
            </svg>
          </section>
        </Element>

        <Element name="help">
          <section className=" py-5 pb-0 black mt-0">
            <div className="row text-center pt-5">
              <div className="col-8 text-white py-4">
                <h1>Take Help</h1>
              </div>
              <div className="col-12 col-md-4 text-white py-4  ">
                <h1>Give Help</h1>
              </div>
            </div>

            <div className="row justify-content-center pb-5">
              <div className="col-8">
                <form>
                  <div className="form-row">
                    <div class="form-group col-12 col-md-5">
                      <input
                        class="form-control"
                        name="name"
                        placeholder="Enter your full name /नाम दर्ज करें
                      "
                        onChange={this.handleInputChange}
                      ></input>
                    </div>
                    <div class="form-group col-12 col-md-5">
                      <input
                        class="form-control"
                        name="adhar"
                        placeholder="Enter your ADHAR Number/आधार नंबर दर्ज करें"
                        onChange={this.handleInputChange}
                      ></input>
                    </div>

                    <div class="form-group col-12 col-md-5 mr-5">
                      <select
                        class="form-control"
                        id="category"
                        name="status"
                        onChange={this.handleInputChange}
                      >
                        <option selected>{this.state.status}(Selected)</option>
                        {this.state.category.map((data, _i) => (
                          <>
                            <option>{data}</option>
                          </>
                        ))}
                      </select>
                    </div>
                    <div class="custom-file col-12 col-md-4 ml-4">
                      <input
                        type="file"
                        name="image"
                        enctype="multipart/form-data"
                        onChange={this.handleFileUpload}
                        placeholder="Upload Image"
                        aria-describedby="inputGroupFileAddon01"
                      ></input>
                      <label
                        class="custom-file-label"
                        for="inputGroupFile01"
                      ></label>
                    </div>
                    <div class="form-group col-12 col-md-10">
                      <input
                        class="form-control mobile"
                        name="mobile"
                        placeholder="Enter your mobile Number/मोबाइल नंबर दर्ज करें"
                        onChange={this.handleInputChange}
                      ></input>
                    </div>
                    <div class="form-group col-12 col-md-8">
                      <textarea
                        class="form-control"
                        name="caption"
                        rows="3"
                        onChange={this.handleInputChange}
                        placeholder="Mention / वर्णन"
                      ></textarea>
                    </div>
                    <div class="form-group col-12 col-md-2">
                      <textarea
                        class="form-control"
                        name="address"
                        rows="3"
                        onChange={this.handleInputChange}
                        placeholder="Address / पता"
                      ></textarea>
                    </div>
                  </div>
                  <div className="col-10">
                    <button
                      type="button"
                      class="btn btn-primary btn-lg btn-block "
                      onClick={this.onSubmit.bind(this)}
                      href="/posts"
                    >
                      <a href="/posts" className="text-white ">
                        Submit / जमा करे
                      </a>
                    </button>
                  </div>
                </form>
              </div>
              <div className="col-12 col-md-2 text-white justify-content-center">
                <div className="row justify-content-center">
                  <div className="col-12 mx-auto">
                    <img src="/img/pm.png" alt="."></img>
                  </div>
                </div>
                <div className="row text-center">
                  <div className="col">
                    <small className="">Link to PM Relief fund</small>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </Element>
        <Element name="spread">
          <section>
            <div className="container">
              <div className="py-5">
                <h1 className="text-dark text-center">The Corona Spread</h1>
                {this.state.loading ? (
                  <div className="text-center">
                    <img src="/img/loader.gif" alt=""></img>
                  </div>
                ) : (
                  <div className="row">
                    <div className="col-md-8">
                      <table class="table table-striped mb-0 table-borderless text-center bg-white">
                        <thead>
                          <tr>
                            <th scope="col" colSpan={4} className="">
                              <div className="py-3"></div>
                            </th>
                          </tr>
                          <tr className="border">
                            <th scope="col">Country</th>
                            <th scope="col">Total Cases</th>
                            <th scope="col">Total Deaths</th>
                            <th scope="col">New Cases</th>

                            <th scope="col">New Deaths</th>
                            <th scope="col">Total Recovered</th>
                          </tr>
                        </thead>

                        <tbody>
                          {this.state.count.map((data, i) => (
                            <tr>
                              <td>{data.country}</td>
                              <td>{data.total_cases}</td>
                              <td
                                className="border text-white"
                                style={{ backgroundColor: "#a83737" }}
                              >
                                {data.total_deaths}
                              </td>
                              <td>{data.fresh_cases}</td>

                              <td>{data.fresh_deaths}</td>
                              <td
                                className="border text-white"
                                style={{ backgroundColor: "#37a86f" }}
                              >
                                {data.total_recovered}
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>

                    <div className="col-12 col-md-4 mt-5 border shadow-sm text-dark text-center">
                      <h2 className="pt-3">News</h2>
                      <div style={{ overflowY: "scroll", height: "22rem" }}>
                        {this.state.news.map((news, key) => (
                          <div>
                            <a href={news[1]}>
                              <li>{news[0]}</li>
                            </a>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                )}
              </div>
              <div className="text-dark justify-content-end">
                <small>As per {this.state.prevdate.slice(0, 10)}</small>
                <small>
                  <a href="https://www.mohfw.gov.in/pdf/coronvavirushelplinenumber.pdf">
                    {" "}
                    || Helplines
                  </a>
                </small>
              </div>
            </div>
          </section>
        </Element>
      </div>
    );
  }
}

export default Home;
