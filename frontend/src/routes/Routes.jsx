// @flow
import React, { PureComponent, Suspense, lazy } from "react";
import { Switch, Route, withRouter } from "react-router-dom";
import AppContainer from "../layouts/app";
import { Loader } from "../components";
import Home from "./Home";

import FourOhFour from "./Home/FourOhFour";

type Props = {
  location: {
    pathname: string,
  },
};

class Routes extends PureComponent<Props> {
  componentDidUpdate(prevProps: Props) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      window.scrollTo({
        top: 0,
        left: 0,
        behavior: "smooth",
      });
    }
  }

  render() {
    const routes = [
      {
        title: "Eyantra",
        path: "/",
        exact: true,
        component: Home,
      },
      {
        title: "Posts",
        path: "/posts",
        exact: true,
        component: () => import("./Posts"),
      },
    ];

    return (
      <Suspense fallback={<Loader />}>
        <AppContainer>
          <Switch>
            <Route {...routes[0]} />
            {routes.splice(1).map(({ title, path, exact, component }, i) => (
              <Route
                key={i}
                exact={exact}
                path={path}
                component={component ? lazy(component) : FourOhFour}
              />
            ))}
            <Route component={FourOhFour} />
          </Switch>
        </AppContainer>
      </Suspense>
    );
  }
}

export default withRouter(Routes);
