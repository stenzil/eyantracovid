import React, { Component } from "react";
import { Navbar } from "../../components";
import { Link } from "react-router-dom";
import { Link as ScrollLink } from "react-scroll";
import "./styles.scss";

class Posts extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      posts: [],
      complains: [],
    };
    this.reverse = this.reverse.bind(this);
  }
  async handleUpvote(e) {
    var url = "http://localhost:8000/posts/upvote/" + e;
    const response = await fetch(url);
    const data = await response.json();
    console.log(data);
  }
  async handleTrivial(e) {
    var url = "http://localhost:8000/posts/trivial/" + e;
    const response = await fetch(url);
    const data = await response.json();
    console.log(data);
  }
  async handleUpvoteC(e) {
    var url = "http://localhost:8000/cmp/upvote/" + e;
    const response = await fetch(url);
    const data = await response.json();
    console.log(data);
  }
  async handleTrivialC(e) {
    var url = "http://localhost:8000/cmp/trivial/" + e;
    const response = await fetch(url);
    const data = await response.json();
    console.log(data);
  }
  reverse(event) {
    //const revpost = this.state.posts.slice().reverse();
    // const { postlist } = this.state.posts;
    // let newpostlist = postlist.reverse();
  }
  toggleSortDate(event) {
    const postlist = this.state.posts;
    this.setState({
      posts: postlist.sort((a, b) => a.createdAt > b.createdAt),
    });
    console.log(this.state.posts);
  }
  componentDidUpdate(nextState, prevState) {}
  async componentDidMount() {
    const url = "http://localhost:8000/posts/";
    const response = await fetch(url);
    const data = await response.json();

    const urlC = "http://localhost:8000/cmp/";
    const responseC = await fetch(urlC);
    const dataC = await responseC.json();
    this.setState({
      posts: data,
      complains: dataC,
      loading: false,
    });
  }
  render() {
    return (
      <div>
        {this.state.loading ? (
          <div className="justify-content-center align-item-center text-center align-middle">
            <img src="/img/loader.gif" alt=".."></img>
          </div>
        ) : (
          <div>
            <Navbar>
              <ul className=" d-flex align-items-around navbar-nav ml-auto text-capitalize text-white ">
                <li>
                  <Link to="/about" className="d-md-block d-none nav-link ml-3">
                    Education
                  </Link>
                </li>
                <li className="nav-item pl-4 d-lg-block d-none">
                  <ScrollLink
                    className="nav-link text-white "
                    to="complain"
                    smooth
                  >
                    Complain
                  </ScrollLink>
                </li>
                <li className="d-lg-block d-none">
                  <a
                    className="nav-link  ml-4"
                    href="http://13.127.125.11/blog/"
                  >
                    Blog
                  </a>
                </li>
                <li>
                  <ScrollLink
                    to="help"
                    smooth
                    className="d-lg-block d-none nav-link btn btn-primary rounded-pill border-white bg-transparent px-4 ml-4"
                  >
                    Give Help
                  </ScrollLink>
                </li>
                <li className="nav-item ml-4">
                  <ScrollLink
                    className="d-lg-block d-none text-dark px-4 btn btn-primary text-capitalize rounded-pill shadow bg-white border-white "
                    to="help"
                    smooth
                  >
                    Need Help ?
                  </ScrollLink>
                </li>
              </ul>
            </Navbar>
            <section className="banner min-vh-100">
              <div className="container pt-5">
                <div className="row mt-5 black">
                  <div
                    className="col-6 py-4 border shadow"
                    style={{ overflowY: "scroll", height: "50rem" }}
                  >
                    <div className="text-white text-center pb-3">
                      <h2 className="border shadow">Problems</h2>
                    </div>
                    <form>
                      <div class="form-row">
                        <div class="form-group col-12 col-md-6 mr-5 text-white">
                          <ScrollLink
                            to="help"
                            smooth
                            className=" btn btn-primary rounded-pill border-info bg-transparent px-3 mx-2"
                          >
                            Recent
                          </ScrollLink>
                          <ScrollLink
                            to="help"
                            smooth
                            className=" btn btn-primary rounded-pill border-info bg-transparent px-3 "
                          >
                            Near me
                          </ScrollLink>
                        </div>
                      </div>
                    </form>
                    {this.state.posts.map((data) => (
                      <div>
                        <div class="card mb-3" style={{ maxWidth: "33.75rem" }}>
                          <div class="row no-gutters">
                            <div class="col-md-3">
                              <div class="card-body">
                                <h5 class="card-title">{data.name}</h5>

                                <p class="card-text">
                                  <small class="text-muted">
                                    Last updated at{" "}
                                    {data.updatedAt.slice(11, 19)}
                                  </small>
                                </p>
                                <div className="col-3 text-center align-bottom pl-0 pb-2">
                                  <button
                                    type="button"
                                    class="btn btn-success wi"
                                    onClick={() => this.handleUpvote(data._id)}
                                  >
                                    Upvote
                                  </button>
                                </div>
                                <div className="col-3 text-center pl-0">
                                  <button
                                    type="button"
                                    class="btn btn-danger wi"
                                    onClick={() => this.handleTrivial(data._id)}
                                  >
                                    Trivial
                                  </button>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-3 mt-4">
                              <img
                                src="https://m.economictimes.com/thumb/msid-66226877,width-1200,height-900,resizemode-4,imgsize-97384/india-ranks-103-on-global-hunger-index.jpg"
                                class="card-img avatar"
                                alt="..."
                              ></img>
                            </div>
                            <div class="col-md-6 pr-0">
                              <div class="card-body pr-0">
                                <h5 class="card-title">{data.name}</h5>
                                <p class="card-text">{data.caption}</p>
                                <p class="card-text">
                                  {data.adharno} : Adhar No.
                                </p>
                                <p class="card-text">
                                  {data.mobile} : Mobile No.
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                  <div
                    className="col-6 text-white text-center py-4 bg-dark border shadow-lg "
                    style={{ overflowY: "scroll", height: "50rem" }}
                  >
                    <h2 className="border shadow">Complaints</h2>
                    <form>
                      <div class="form-row">
                        <div class="form-group col-12 col-md-6 ml-0 pl-0 mr-5 text-white mt-3">
                          <ScrollLink
                            to="help"
                            smooth
                            className=" btn btn-primary rounded-pill border-info bg-transparent px-3 mx-2"
                          >
                            Recent
                          </ScrollLink>
                          <ScrollLink
                            to="help"
                            smooth
                            className=" btn btn-primary rounded-pill border-info bg-transparent px-3 "
                          >
                            Near me
                          </ScrollLink>
                        </div>
                      </div>
                    </form>
                    {this.state.complains.map((data) => (
                      <div>
                        <div class="card mb-3" style={{ maxWidth: "33.75rem" }}>
                          <div class="row no-gutters">
                            <div class="col-md-3">
                              <div class="card-body">
                                <small class="card-title text-dark">
                                  {data.area}
                                </small>
                              </div>
                            </div>
                            <div class="col-md-3 mt-3">
                              <img
                                src="https://images.pexels.com/photos/977010/pexels-photo-977010.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                                class="card-img avatar"
                                alt="..."
                              ></img>
                            </div>
                            <div class="col-md-6 pr-0">
                              <div class="card-body pr-0">
                                <p class="card-text text-danger">
                                  {data.caption}
                                </p>
                              </div>
                              <div className="row justify-content-around">
                                <div className="col-3 text-center align-bottom pl-0 pb-2">
                                  <button
                                    type="button"
                                    class="btn btn-success wi"
                                    onClick={() => this.handleUpvoteC(data._id)}
                                  >
                                    Upvote
                                  </button>
                                </div>
                                <div className="col-3 text-center pl-0">
                                  <button
                                    type="button"
                                    class="btn btn-danger wi"
                                    onClick={() =>
                                      this.handleTrivialC(data._id)
                                    }
                                  >
                                    Trivial
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </section>
          </div>
        )}
      </div>
    );
  }
}
export default Posts;
