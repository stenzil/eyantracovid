// @flow
import React from "react";

import "rc-slider/assets/index.css";
import "react-circular-progressbar/dist/styles.css";
import "./styles.scss";

type Props = {
  heading: string,
  avatar: string,
  description: string,
};
const FeatureCard = ({ heading, avatar, description }: Props) => {
  return (
    <div className="card border-0 mt-4 shadow mx-4">
      <div className="card-body rounded">
        <div className="d-flex align-items-center">
          <div className="m-2 ">
            <div className="row align-items-center">
              <div className="col-12 text-center">
                <img className="d-block mx-auto rounded" src={avatar} alt="" />
              </div>
            </div>
            <div className="row  text-dark text-center">
              <h5 className="py-2 font-weight-normal">{heading}</h5>
              <p className="nunito">{description}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeatureCard;
