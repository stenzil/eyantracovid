## Title of the Project : -
   WeHumans
## Description : -
  This project aims towards bringing the entire fuss to one platform.
  Be it for asking support, giving support or complaint services against the bottlenecks.
  This project allows people to express their problem to the entire world, and let people help them directly by financial or supply aid, There's also a QR code for PM relief fund donation.
  this allows a common man to find all the information regarding the spread across the globe and how the world is responding to the virus via tweets.
  Tweets by Narendra Modi are shown on the website and all those with COVID are also show. Top news around is also show along with the figures of spread in various countires
  A small link for all emergency helplines has also been included
## Software Requirements : -
  Backend : Nodejs
  Frontend : Reactjs
## Process Flow : -
  I started building the backend first, which included gathering the tweets by user based and caption based search.
  Web scrapping was used gather the counts all across globe. Mongoose Atlas has been used to store the data for the posts and complaints
  Keeping the backend light and smooth we've used a simple route and model structure.
  Frontend has been developed then using reactjs and made fully responsive, bootstrap has been used for designing. Loaders and other components well build.


![Scheme](dfd.png)

## How to use : 
  1. Clone the repo : 
  2. Open backend folder : 
    2.1 run "npm i "
    2.2 run "nodemon server"
  3. open frontend folder in different window of IDE
    3.1 run "npm i"
    3.2 run "npm start"
  
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
